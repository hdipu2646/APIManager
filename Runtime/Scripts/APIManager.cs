using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using Walid.Singleton;
using Walid.UI;

namespace Walid.System
{
    public class UnityWebRequestAwaiter : INotifyCompletion
    {
        private UnityWebRequestAsyncOperation asyncOp;
        private Action continuation;

        public UnityWebRequestAwaiter(UnityWebRequestAsyncOperation asyncOp)
        {
            this.asyncOp = asyncOp;
            asyncOp.completed += OnRequestCompleted;
        }

        public bool IsCompleted { get { return asyncOp.isDone; } }

        public void GetResult() { }

        public void OnCompleted(Action continuation)
        {
            this.continuation = continuation;
        }

        private void OnRequestCompleted(AsyncOperation obj)
        {
            continuation();
        }
    }

    public static class ExtensionMethods
    {
        public static UnityWebRequestAwaiter GetAwaiter(this UnityWebRequestAsyncOperation asyncOp)
        {
            return new UnityWebRequestAwaiter(asyncOp);
        }
    }

    /*
    // Usage example:
    UnityWebRequest www = new UnityWebRequest();
    // ...
    await www.SendWebRequest();
    Debug.Log(req.downloadHandler.text);
    */
}

namespace Walid.System.API
{
    public class APIManager : SingletonTemplate<APIManager>
    {
        public ProgressBar progressBar;

        #region Public Methods

        #region Coroutines

        #region Post Methods
        public IEnumerator PostFile(string endPoint, string accessToken, List<IMultipartFormSection> formData, string fileName, bool showDebug, Action<bool, long, string, string> callBack)
        {
            if (showDebug)
                Debug.Log(string.Format("Post File to Endpoint:-\n{0}\n\n", endPoint));
            using (UnityWebRequest uwr = UnityWebRequest.Post(endPoint, formData))
            {
                uwr.disposeCertificateHandlerOnDispose = uwr.disposeDownloadHandlerOnDispose = uwr.disposeUploadHandlerOnDispose = true;
                progressBar.gameObject.SetActive(true);
                uwr.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                yield return new WaitForEndOfFrame(); //For Activate Loading Bar. Without it loading bar not Wokring.
                UnityWebRequestAsyncOperation asyncOperation = uwr.SendWebRequest();
                double fileSize = uwr.uploadHandler.data.LongLength;
                fileSize = Math.Round(fileSize / 1024 / 1024, 2);
                while (uwr.result == UnityWebRequest.Result.InProgress)
                {
                    progressBar.Value = asyncOperation.progress;
                    double uploadedSize = uwr.uploadedBytes;
                    progressBar.statusText.text = string.Format("{0}'s file is uploading {1} MB of {2} MB to server", fileName, Math.Round((uploadedSize / 1024 / 1024), 2), fileSize);
                    yield return new WaitForEndOfFrame();
                }
                /// Avoiding UnityWebRequest.Result.InProgress 
                Action apiCallBack = uwr.result switch
                {
                    UnityWebRequest.Result.ConnectionError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Connection Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.ProtocolError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Protocol Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.DataProcessingError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.Success => () =>
                    {
                        try
                        {
                            string response = Encoding.UTF8.GetString(uwr.downloadHandler.data);
                            if (showDebug)
                                Debug.Log(string.Format("Post File to Endpoint:-\n{0}\n\n is successfull", endPoint));
                            callBack?.Invoke(false, uwr.responseCode, string.Format("File - {0} Uploaded Successfully", fileName), response);
                        }
                        catch (Exception exception)
                        {
                            callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", "Response Parse Error on Client Side");
                            if (showDebug)
                                Debug.LogError(exception);
                        }
                    }
                    ,
                    _ => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with unknown error\nGet Request - Endpoint \n\n{0}\n\n is hitted\n\n Response:-\n\n{1}\n\n", endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Unknown Error", "Unknown Error");
                    }
                };
                progressBar.Reset();
                apiCallBack?.Invoke();
                uwr.Dispose();
            }
        }
        public IEnumerator PostFile(string endPoint, string accessToken, List<IMultipartFormSection> formData, string fileName, ProgressBar progressBar, bool showDebug, Action<bool, long, string, string> callBack)
        {
            if (showDebug)
                Debug.Log(string.Format("Post File to Endpoint:-\n{0}\n\n", endPoint));
            using (UnityWebRequest uwr = UnityWebRequest.Post(endPoint, formData))
            {
                uwr.disposeCertificateHandlerOnDispose = uwr.disposeDownloadHandlerOnDispose = uwr.disposeUploadHandlerOnDispose = true;
                progressBar.gameObject.SetActive(true);
                uwr.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                yield return new WaitForEndOfFrame(); //For Activate Loading Bar. Without it loading bar not Wokring.
                UnityWebRequestAsyncOperation asyncOperation = uwr.SendWebRequest();
                double fileSize = uwr.uploadHandler.data.LongLength;
                fileSize = Math.Round(fileSize / 1024 / 1024, 2);
                while (uwr.result == UnityWebRequest.Result.InProgress)
                {
                    progressBar.Value = asyncOperation.progress;
                    double uploadedSize = uwr.uploadedBytes;
                    progressBar.statusText.text = string.Format("{0}'s file is uploading {1} MB of {2} MB to server", fileName, Math.Round((uploadedSize / 1024 / 1024), 2), fileSize);
                    yield return new WaitForEndOfFrame();
                }
                /// Avoiding UnityWebRequest.Result.InProgress 
                Action apiCallBack = uwr.result switch
                {
                    UnityWebRequest.Result.ConnectionError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Connection Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.ProtocolError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Protocol Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.DataProcessingError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.Success => () =>
                    {
                        try
                        {
                            string response = Encoding.UTF8.GetString(uwr.downloadHandler.data);
                            if (showDebug)
                                Debug.Log(string.Format("Post File to Endpoint:-\n{0}\n\n is successfull", endPoint));
                            callBack?.Invoke(false, uwr.responseCode, "Synchronized Successfully", response);
                        }
                        catch (Exception exception)
                        {
                            callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", "Response Parse Error on Client Side");
                            if (showDebug)
                                Debug.LogError(exception);
                        }
                    }
                    ,
                    _ => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with unknown error\nGet Request - Endpoint \n\n{0}\n\n is hitted\n\n Response:-\n\n{1}\n\n", endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Unknown Error", "Unknown Error");
                    }
                };
                progressBar.Reset();
                apiCallBack?.Invoke();
                uwr.Dispose();
            }
        }
        public IEnumerator PostJsonRequest(string endPoint, string accessToken, string jsonString, string dataName, bool showDebug, Action<bool, long, string, string> callBack)
        {
            if (showDebug)
                Debug.Log(string.Format("Post Request Endpoint:-\n{0}\nHitted with JSON:-\n\n{1}\n\n", endPoint, jsonString));
            using (UnityWebRequest uwr = UnityWebRequest.PostWwwForm(endPoint, jsonString))
            {
                uwr.disposeCertificateHandlerOnDispose = uwr.disposeDownloadHandlerOnDispose = uwr.disposeUploadHandlerOnDispose = true;
                progressBar.gameObject.SetActive(true);
                uwr.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                uwr.SetRequestHeader("Content-Type", "application/json");
                uwr.SetRequestHeader("Accept", "application/json");
                uwr.uploadHandler.contentType = "application/json";
                yield return new WaitForEndOfFrame(); //For Activate Loading Bar. Without it loading bar not Wokring.
                uwr.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(jsonString));
                UnityWebRequestAsyncOperation asyncOperation = uwr.SendWebRequest();
                double fileSize = uwr.uploadHandler.data.LongLength;
                fileSize = Math.Round(fileSize / 1024, 2);
                while (uwr.result == UnityWebRequest.Result.InProgress)
                {
                    progressBar.Value = asyncOperation.progress;
                    double uploadedSize = uwr.uploadedBytes;
                    progressBar.statusText.text = string.Format("{0}'s data is saving to server", dataName);
                    yield return new WaitForEndOfFrame();
                }
                /// Avoiding UnityWebRequest.Result.InProgress 
                Action apiCallBack = uwr.result switch
                {
                    UnityWebRequest.Result.ConnectionError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Connection Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.ProtocolError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Protocol Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.DataProcessingError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.Success => () =>
                    {
                        try
                        {
                            jsonString = Encoding.UTF8.GetString(uwr.downloadHandler.data);
                            if (showDebug)
                                Debug.Log(string.Format("Post Request - Endpoint \n\n{0}\n\n is hitted - Received Json\n\n{1}\n\n", endPoint, jsonString));
                            callBack?.Invoke(false, uwr.responseCode, string.Format("{0} is complete", dataName), jsonString);
                        }
                        catch (Exception exception)
                        {
                            callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", "Response Parse Error on Client Side");
                            if (showDebug)
                                Debug.LogError(exception);
                        }
                    }
                    ,
                    _ => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with unknown error\nGet Request - Endpoint \n\n{0}\n\n is hitted\n\n Response:-\n\n{1}\n\n", endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Unknown Error", "Unknown Error");
                    }
                    ,
                };
                progressBar.Reset();
                apiCallBack?.Invoke();
                uwr.Dispose();
            }
        }
        public IEnumerator PostJsonRequest(string endPoint, string accessToken, string jsonString, string dataName, ProgressBar progressBar, bool showDebug, Action<bool, long, string, string> callBack)
        {
            if (showDebug)
                Debug.Log(string.Format("Post Request Endpoint:-\n{0}\nHitted with JSON:-\n\n{1}\n\n", endPoint, jsonString));
            using (UnityWebRequest uwr = UnityWebRequest.PostWwwForm(endPoint, jsonString))
            {
                uwr.disposeCertificateHandlerOnDispose = uwr.disposeDownloadHandlerOnDispose = uwr.disposeUploadHandlerOnDispose = true;
                progressBar.gameObject.SetActive(true);
                uwr.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                uwr.SetRequestHeader("Content-Type", "application/json");
                uwr.SetRequestHeader("Accept", "application/json");
                uwr.uploadHandler.contentType = "application/json";
                yield return new WaitForEndOfFrame(); //For Activate Loading Bar. Without it loading bar not Wokring.
                uwr.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(jsonString));
                UnityWebRequestAsyncOperation asyncOperation = uwr.SendWebRequest();
                double fileSize = uwr.uploadHandler.data.LongLength;
                fileSize = Math.Round(fileSize / 1024, 2);
                while (uwr.result == UnityWebRequest.Result.InProgress)
                {
                    progressBar.Value = asyncOperation.progress;
                    double uploadedSize = uwr.uploadedBytes;
                    progressBar.statusText.text = string.Format("{0}'s data is saving to server", dataName);
                    yield return new WaitForEndOfFrame();
                }
                /// Avoiding UnityWebRequest.Result.InProgress 
                Action apiCallBack = uwr.result switch
                {
                    UnityWebRequest.Result.ConnectionError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Connection Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.ProtocolError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Protocol Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.DataProcessingError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.Success => () =>
                    {
                        try
                        {
                            jsonString = Encoding.UTF8.GetString(uwr.downloadHandler.data);
                            if (showDebug)
                                Debug.Log(string.Format("Post Request - Endpoint \n\n{0}\n\n is hitted - Received Json\n\n{1}\n\n", endPoint, jsonString));
                            callBack?.Invoke(false, uwr.responseCode, string.Format("{0} is complete", dataName), jsonString);
                        }
                        catch (Exception exception)
                        {
                            callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", "Response Parse Error on Client Side");
                            if (showDebug)
                                Debug.LogError(exception);
                        }
                    }
                    ,
                    _ => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with unknown error\nGet Request - Endpoint \n\n{0}\n\n is hitted\n\n Response:-\n\n{1}\n\n", endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Unknown Error", "Unknown Error");
                    }
                    ,
                };
                progressBar.Reset();
                apiCallBack?.Invoke();
                uwr.Dispose();
            }
        }
        #endregion Post Methods

        #region Put Methods
        public IEnumerator PutJsonRequest(string endPoint, string accessToken, string jsonString, string dataName, bool showDebug, Action<bool, long, string, string> callBack)
        {
            if (showDebug)
                Debug.Log(string.Format("Put Request Endpoint:-\n{0}\nHitted with JSON:-\n\n{1}\n\n", endPoint, jsonString));
            using (UnityWebRequest uwr = UnityWebRequest.Put(endPoint, jsonString))
            {
                uwr.disposeCertificateHandlerOnDispose = uwr.disposeDownloadHandlerOnDispose = uwr.disposeUploadHandlerOnDispose = true;
                //uwr.method = string.Format("PATCH");
                progressBar.gameObject.SetActive(true);
                uwr.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                uwr.SetRequestHeader("Content-Type", "application/json");
                uwr.SetRequestHeader("Accept", "application/json");
                uwr.uploadHandler.contentType = "application/json";
                yield return new WaitForEndOfFrame();//For Activate Loading Bar. Without it loading bar not Wokring.
                uwr.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(jsonString));
                UnityWebRequestAsyncOperation asyncOperation = uwr.SendWebRequest();
                double fileSize = uwr.uploadHandler.data.LongLength;
                fileSize = Math.Round(fileSize / 1024, 2);
                while (uwr.result == UnityWebRequest.Result.InProgress)
                {
                    progressBar.Value = asyncOperation.progress;
                    double uploadedSize = uwr.uploadedBytes;
                    progressBar.statusText.text = string.Format("{0}'s data is updating to server", dataName);
                    yield return new WaitForEndOfFrame();
                }
                /// Avoiding UnityWebRequest.Result.InProgress 
                Action apiCallBack = uwr.result switch
                {
                    UnityWebRequest.Result.ConnectionError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Connection Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.ProtocolError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Protocol Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.DataProcessingError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.Success => () =>
                    {
                        try
                        {
                            jsonString = Encoding.UTF8.GetString(uwr.downloadHandler.data);
                            if (showDebug)
                                Debug.Log(string.Format("Put Request - Endpoint \n\n{0}\n\n is hitted - Received Json\n\n{1}\n\n", endPoint, jsonString));
                            callBack?.Invoke(false, uwr.responseCode, string.Format("{0} is complete", dataName), jsonString);
                        }
                        catch (Exception exception)
                        {
                            callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", "Response Parse Error on Client Side");
                            if (showDebug)
                                Debug.LogError(exception);
                        }
                    }
                    ,
                    _ => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with unknown error\nGet Request - Endpoint \n\n{0}\n\n is hitted\n\n Response:-\n\n{1}\n\n", endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Unknown Error", "Unknown Error");
                    }
                    ,
                };
                progressBar.Reset();
                apiCallBack?.Invoke();
                uwr.Dispose();
            }
        }
        public IEnumerator PutJsonRequest(string endPoint, string accessToken, string jsonString, string dataName, ProgressBar progressBar, bool showDebug, Action<bool, long, string, string> callBack)
        {
            if (showDebug)
                Debug.Log(string.Format("Put Request Endpoint:-\n{0}\nHitted with JSON:-\n\n{1}\n\n", endPoint, jsonString));
            using (UnityWebRequest uwr = UnityWebRequest.Put(endPoint, jsonString))
            {
                uwr.disposeCertificateHandlerOnDispose = uwr.disposeDownloadHandlerOnDispose = uwr.disposeUploadHandlerOnDispose = true;
                //uwr.method = string.Format("PATCH");
                progressBar.gameObject.SetActive(true);
                uwr.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                uwr.SetRequestHeader("Content-Type", "application/json");
                uwr.SetRequestHeader("Accept", "application/json");
                uwr.uploadHandler.contentType = "application/json";
                yield return new WaitForEndOfFrame();//For Activate Loading Bar. Without it loading bar not Wokring.
                uwr.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(jsonString));
                UnityWebRequestAsyncOperation asyncOperation = uwr.SendWebRequest();
                double fileSize = uwr.uploadHandler.data.LongLength;
                fileSize = Math.Round(fileSize / 1024, 2);
                while (uwr.result == UnityWebRequest.Result.InProgress)
                {
                    progressBar.Value = asyncOperation.progress;
                    double uploadedSize = uwr.uploadedBytes;
                    progressBar.statusText.text = string.Format("{0}'s data is updating to server", dataName);
                    yield return new WaitForEndOfFrame();
                }
                /// Avoiding UnityWebRequest.Result.InProgress 
                Action apiCallBack = uwr.result switch
                {
                    UnityWebRequest.Result.ConnectionError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Connection Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.ProtocolError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Protocol Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.DataProcessingError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.Success => () =>
                    {
                        try
                        {
                            jsonString = Encoding.UTF8.GetString(uwr.downloadHandler.data);
                            if (showDebug)
                                Debug.Log(string.Format("Put Request - Endpoint \n\n{0}\n\n is hitted - Received Json\n\n{1}\n\n", endPoint, jsonString));
                            callBack?.Invoke(false, uwr.responseCode, string.Format("{0} is complete", dataName), jsonString);
                        }
                        catch (Exception exception)
                        {
                            callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", "Response Parse Error on Client Side");
                            if (showDebug)
                                Debug.LogError(exception);
                        }
                    }
                    ,
                    _ => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with unknown error\nGet Request - Endpoint \n\n{0}\n\n is hitted\n\n Response:-\n\n{1}\n\n", endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Unknown Error", "Unknown Error");
                    }
                    ,
                };
                progressBar.Reset();
                apiCallBack?.Invoke();
                uwr.Dispose();
            }
        }
        #endregion Put Methods

        #region Get Methods
        public IEnumerator GetFile(string endPoint, string accessToken, string fileName, bool showDebug, Action<bool, long, string, string, byte[]> callBack)
        {
            if (showDebug)
                Debug.Log(string.Format("Get File from - Endpoint \n\n{0}\n\n is hitted", endPoint));
            using (UnityWebRequest uwr = UnityWebRequest.Get(endPoint))
            {
                uwr.disposeCertificateHandlerOnDispose = uwr.disposeDownloadHandlerOnDispose = uwr.disposeUploadHandlerOnDispose = true;
                progressBar.gameObject.SetActive(true);
                progressBar.statusText.text = string.Format("Downloading {0}",fileName);
                uwr.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                yield return new WaitForEndOfFrame(); //For Activate Loading Bar. Without it loading bar not Wokring.
                double fileSize = 0;
                using (UnityWebRequest request = UnityWebRequest.Head(endPoint))
                {
                    request.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                    yield return request.SendWebRequest();
                    double.TryParse(request.GetResponseHeader("Content-Length"),out fileSize);
                    request.Dispose();
                }
                fileSize = Math.Round(fileSize / 1024 / 1024, 2);
                UnityWebRequestAsyncOperation asyncOperation = uwr.SendWebRequest();
                while (uwr.result == UnityWebRequest.Result.InProgress)
                {
                    progressBar.Value = uwr.downloadProgress;
                    double downloadedSize = uwr.downloadedBytes;
                    progressBar.statusText.text = string.Format("{0}'s file is downloading {1} MB of {2} MB from server", fileName, Math.Round((downloadedSize / 1024 / 1024), 2), fileSize);
                    yield return new WaitForEndOfFrame();
                }
                /// Avoiding UnityWebRequest.Result.InProgress 
                Action apiCallBack = uwr.result switch
                {
                    UnityWebRequest.Result.ConnectionError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Connection Error", uwr.error, null);
                    }
                    ,
                    UnityWebRequest.Result.ProtocolError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Protocol Error", uwr.error, null);
                    }
                    ,
                    UnityWebRequest.Result.DataProcessingError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", uwr.error, null);
                    }
                    ,
                    UnityWebRequest.Result.Success => () =>
                    {
                        try
                        {
                            byte[] data = uwr.downloadHandler.data;
                            if (showDebug)
                                Debug.Log(string.Format("Get File from Endpoint:-\n{0}\n\n is successfull", endPoint));
                            callBack?.Invoke(false, uwr.responseCode, "Successful", "File Downloaded Successfully", data);
                        }
                        catch (Exception exception)
                        {
                            callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", "Response Parse Error on Client Side", null);
                            if (showDebug)
                                Debug.LogError(exception);
                        }
                    }
                    ,
                    _ => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with unknown error\nGet Request - Endpoint \n\n{0}\n\n is hitted\n\n Response:-\n\n{1}\n\n", endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Unknown Error", "Unknown Error", null);
                    }
                };
                progressBar.Reset();
                apiCallBack?.Invoke();
                uwr.Dispose();
            }
        }
        public IEnumerator GetFile(string endPoint, string accessToken, string fileName, ProgressBar progressBar, bool showDebug, Action<bool, long, string, string, byte[]> callBack)
        {
            if (showDebug)
                Debug.Log(string.Format("Get File from - Endpoint \n\n{0}\n\n is hitted", endPoint));
            using (UnityWebRequest uwr = UnityWebRequest.Get(endPoint))
            {
                uwr.disposeCertificateHandlerOnDispose = uwr.disposeDownloadHandlerOnDispose = uwr.disposeUploadHandlerOnDispose = true;
                progressBar.gameObject.SetActive(true);
                progressBar.statusText.text = string.Format("Downloading {0}", fileName);
                uwr.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                yield return new WaitForEndOfFrame(); //For Activate Loading Bar. Without it loading bar not Wokring.
                double fileSize = 0;
                using (UnityWebRequest request = UnityWebRequest.Head(endPoint))
                {
                    request.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                    yield return request.SendWebRequest();
                    double.TryParse(request.GetResponseHeader("Content-Length"), out fileSize);
                    request.Dispose();
                }
                fileSize = Math.Round(fileSize / 1024 / 1024, 2);
                UnityWebRequestAsyncOperation asyncOperation = uwr.SendWebRequest();
                while (uwr.result == UnityWebRequest.Result.InProgress)
                {
                    progressBar.Value = uwr.downloadProgress;
                    double downloadedSize = uwr.downloadedBytes;
                    progressBar.statusText.text = string.Format("{{0}'s file is downloading {1} MB of {2} MB from server", fileName, Math.Round((downloadedSize / 1024 / 1024), 2), fileSize);
                    yield return new WaitForEndOfFrame();
                }
                /// Avoiding UnityWebRequest.Result.InProgress 
                Action apiCallBack = uwr.result switch
                {
                    UnityWebRequest.Result.ConnectionError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Connection Error", uwr.error, null);
                    }
                    ,
                    UnityWebRequest.Result.ProtocolError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Protocol Error", uwr.error, null);
                    }
                    ,
                    UnityWebRequest.Result.DataProcessingError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", uwr.error, null);
                    }
                    ,
                    UnityWebRequest.Result.Success => () =>
                    {
                        try
                        {
                            byte[] data = uwr.downloadHandler.data;
                            if (showDebug)
                                Debug.Log(string.Format("Get File from Endpoint:-\n{0}\n\n is successfull", endPoint));
                            callBack?.Invoke(false, uwr.responseCode, "Successful", "File Downloaded Successfully", data);
                        }
                        catch (Exception exception)
                        {
                            callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", "Response Parse Error on Client Side", null);
                            if (showDebug)
                                Debug.LogError(exception);
                        }
                    }
                    ,
                    _ => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with unknown error\nGet Request - Endpoint \n\n{0}\n\n is hitted\n\n Response:-\n\n{1}\n\n", endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Unknown Error", "Unknown Error", null);
                    }
                };
                progressBar.Reset();
                apiCallBack?.Invoke();
                uwr.Dispose();
            }
        }
        public IEnumerator GetJsonResponse(string endPoint, string accessToken, string dataName, bool showDebug, Action<bool, long, string, string> callBack)
        {
            if (showDebug)
                Debug.Log(string.Format("Get Request - Endpoint \n\n{0}\n\n is hitted", endPoint));
            using (UnityWebRequest uwr = UnityWebRequest.Get(endPoint))
            {
                uwr.disposeCertificateHandlerOnDispose = uwr.disposeDownloadHandlerOnDispose = uwr.disposeUploadHandlerOnDispose = true;
                progressBar.gameObject.SetActive(true);
                progressBar.statusText.text = string.Format("Fetching {0}", dataName);
                uwr.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                uwr.SetRequestHeader("Accept", "application/json");
                yield return new WaitForEndOfFrame();//For Activate Loading Bar. Without it loading bar not Wokring.
                UnityWebRequestAsyncOperation asyncOperation = uwr.SendWebRequest();
                while (uwr.result == UnityWebRequest.Result.InProgress)
                {
                    progressBar.Value = asyncOperation.progress;
                    progressBar.statusText.text = string.Format("{0}'s data is downloading from server", dataName);
                    yield return new WaitForEndOfFrame();
                }
                /// Avoiding UnityWebRequest.Result.InProgress 
                Action apiCallBack = uwr.result switch
                {
                    UnityWebRequest.Result.ConnectionError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Connection Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.ProtocolError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Protocol Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.DataProcessingError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.Success => () =>
                    {
                        try
                        {
                            string jsonString = Encoding.UTF8.GetString(uwr.downloadHandler.data);
                            if(showDebug)
                                Debug.Log(string.Format("Get Request - Endpoint \n\n{0}\n\n is hitted - Received Json\n\n{1}\n\n", endPoint, jsonString));
                            callBack?.Invoke(false, uwr.responseCode, string.Format("{0} is complete", dataName), jsonString);
                        }
                        catch (Exception exception)
                        {
                            callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", "Response Parse Error on Client Side");
                            if (showDebug)
                                Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", endPoint, exception, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        }
                    }
                    ,
                    _ => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with unknown error\nGet Request - Endpoint \n\n{0}\n\n is hitted\n\n Response:-\n\n{1}\n\n", endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Unknown Error", "Unknown Error");
                    }
                    ,
                };
                progressBar.Reset();
                apiCallBack?.Invoke();
                uwr.Dispose();
            }
        }
        public IEnumerator GetJsonResponse(string endPoint, string accessToken, string dataName, ProgressBar progressBar, bool showDebug, Action<bool, long, string, string> callBack)
        {
            if (showDebug)
                Debug.Log(string.Format("Get Request - Endpoint \n\n{0}\n\n is hitted", endPoint));
            using (UnityWebRequest uwr = UnityWebRequest.Get(endPoint))
            {
                uwr.disposeCertificateHandlerOnDispose = uwr.disposeDownloadHandlerOnDispose = uwr.disposeUploadHandlerOnDispose = true;
                progressBar.gameObject.SetActive(true);
                progressBar.statusText.text = string.Format("Fetching {0}", dataName);
                uwr.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                uwr.SetRequestHeader("Accept", "application/json");
                yield return new WaitForEndOfFrame();//For Activate Loading Bar. Without it loading bar not Wokring.                
                UnityWebRequestAsyncOperation asyncOperation = uwr.SendWebRequest();
                while (uwr.result == UnityWebRequest.Result.InProgress)
                {
                    progressBar.Value = asyncOperation.progress;
                    progressBar.statusText.text = string.Format("{0}'s data is downloading from server", dataName);
                    yield return new WaitForEndOfFrame();
                }
                /// Avoiding UnityWebRequest.Result.InProgress 
                Action apiCallBack = uwr.result switch
                {
                    UnityWebRequest.Result.ConnectionError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Connection Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.ProtocolError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Protocol Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.DataProcessingError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.Success => () =>
                    {
                        try
                        {
                            string jsonString = Encoding.UTF8.GetString(uwr.downloadHandler.data);
                            if (showDebug)
                                Debug.Log(string.Format("Get Request - Endpoint \n\n{0}\n\n is hitted - Received Json\n\n{1}\n\n", endPoint, jsonString));
                            callBack?.Invoke(false, uwr.responseCode, string.Format("{0} is complete", dataName), jsonString);
                        }
                        catch (Exception exception)
                        {
                            callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", "Response Parse Error on Client Side");
                            if (showDebug)
                                Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", endPoint, exception, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        }
                    }
                    ,
                    _ => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with unknown error\nGet Request - Endpoint \n\n{0}\n\n is hitted\n\n Response:-\n\n{1}\n\n", endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Unknown Error", "Unknown Error");
                    }
                    ,
                };
                progressBar.Reset();
                apiCallBack?.Invoke();
                uwr.Dispose();
            }
        }
        #endregion Get Methods

        #region Delete Methods
        public IEnumerator DeleteData(string endPoint, string accessToken, string dataName, bool showDebug, Action<bool, long, string, string> callBack)
        {
            if (showDebug)
                Debug.Log(string.Format("Delete Request - Endpoint \n\n{0}\n\n is hitted", endPoint));
            using (UnityWebRequest uwr = UnityWebRequest.Delete(endPoint))
            {
                uwr.disposeCertificateHandlerOnDispose = uwr.disposeDownloadHandlerOnDispose = uwr.disposeUploadHandlerOnDispose = true;
                progressBar.gameObject.SetActive(true);
                uwr.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                uwr.SetRequestHeader("Accept", "application/json");
                yield return new WaitForEndOfFrame();//For Activate Loading Bar. Without it loading bar not Wokring.
                UnityWebRequestAsyncOperation asyncOperation = uwr.SendWebRequest();
                while (uwr.result == UnityWebRequest.Result.InProgress)
                {
                    progressBar.Value = asyncOperation.progress;
                    progressBar.statusText.text = string.Format("{0}'s data is deleting on server", dataName);
                    yield return new WaitForEndOfFrame();
                }
                /// Avoiding UnityWebRequest.Result.InProgress 
                Action apiCallBack = uwr.result switch
                {
                    UnityWebRequest.Result.ConnectionError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Connection Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.ProtocolError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Protocol Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.DataProcessingError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.Success => () =>
                    {
                        try
                        {
                            if (uwr.responseCode == 204)
                            {
                                callBack?.Invoke(false, uwr.responseCode, "Delete Success", string.Format("{0} is complete", dataName));
                                if (showDebug)
                                    Debug.Log(string.Format("Delete Data from Endpoint:-\n{0}\n\n is successfull", endPoint));
                            }
                            else
                            {
                                callBack?.Invoke(true, uwr.responseCode, "Unknown Status", "Unknown Status for WebGL");
                                if (showDebug)
                                    Debug.LogWarning(string.Format("Delete Data from Endpoint:-\n{0}\n\n is in Unknown Status", endPoint));
                            }
                        }
                        catch (Exception exception)
                        {
                            callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", "Response Parse Error on Client Side");
                            if (showDebug)
                                Debug.LogError(exception);
                        }
                    }
                    ,
                    _ => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with unknown error\nGet Request - Endpoint \n\n{0}\n\n is hitted\n\n Response:-\n\n{1}\n\n", endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Unknown Error", "Unknown Error");
                    }
                    ,
                };
                progressBar.Reset();
                apiCallBack?.Invoke();
                uwr.Dispose();
            }
        }
        public IEnumerator DeleteData(string endPoint, string accessToken, string dataName, ProgressBar progressBar, bool showDebug, Action<bool, long, string, string> callBack)
        {
            if (showDebug)
                Debug.Log(string.Format("Delete Request - Endpoint \n\n{0}\n\n is hitted", endPoint));
            using (UnityWebRequest uwr = UnityWebRequest.Delete(endPoint))
            {
                uwr.disposeCertificateHandlerOnDispose = uwr.disposeDownloadHandlerOnDispose = uwr.disposeUploadHandlerOnDispose = true;
                progressBar.gameObject.SetActive(true);
                uwr.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                uwr.SetRequestHeader("Accept", "application/json");
                yield return new WaitForEndOfFrame();//For Activate Loading Bar. Without it loading bar not Wokring.
                UnityWebRequestAsyncOperation asyncOperation = uwr.SendWebRequest();
                while (uwr.result == UnityWebRequest.Result.InProgress)
                {
                    progressBar.Value = asyncOperation.progress;
                    progressBar.statusText.text = string.Format("{0}'s data is deleting on server", dataName);
                    yield return new WaitForEndOfFrame();
                }
                /// Avoiding UnityWebRequest.Result.InProgress 
                Action apiCallBack = uwr.result switch
                {
                    UnityWebRequest.Result.ConnectionError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Connection Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.ProtocolError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Protocol Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.DataProcessingError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.Success => () =>
                    {
                        try
                        {
                            if (uwr.responseCode == 204)
                            {
                                callBack?.Invoke(false, uwr.responseCode, "Delete Success", string.Format("{0} is complete", dataName));
                                if (showDebug)
                                    Debug.Log(string.Format("Delete Data from Endpoint:-\n{0}\n\n is successfull", endPoint));
                            }
                            else
                            {
                                callBack?.Invoke(true, uwr.responseCode, "Unknown Status", "Unknown Status for WebGL");
                                if (showDebug)
                                    Debug.LogWarning(string.Format("Delete Data from Endpoint:-\n{0}\n\n is in Unknown Status", endPoint));
                            }
                        }
                        catch (Exception exception)
                        {
                            callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", "Response Parse Error on Client Side");
                            if (showDebug)
                                Debug.LogError(exception);
                        }
                    }
                    ,
                    _ => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with unknown error\nGet Request - Endpoint \n\n{0}\n\n is hitted\n\n Response:-\n\n{1}\n\n", endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Unknown Error", "Unknown Error");
                    }
                    ,
                };
                progressBar.Reset();
                apiCallBack?.Invoke();
                uwr.Dispose();
            }
        }
        #endregion Delete Methods

        #endregion Coroutines

        #region Asynchronous Threads

        #region Post Methods
        public async void PostFileThread(string endPoint, string accessToken, List<IMultipartFormSection> formData, string fileName, bool showDebug, Action<bool, long, string, string> callBack)
        {
            if (showDebug)
                Debug.Log(string.Format("Post File to Endpoint:-\n{0}\n\n", endPoint));
            using (UnityWebRequest uwr = UnityWebRequest.Post(endPoint, formData))
            {
                uwr.disposeCertificateHandlerOnDispose = uwr.disposeDownloadHandlerOnDispose = uwr.disposeUploadHandlerOnDispose = true;
                progressBar.gameObject.SetActive(true);
                uwr.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                await Task.Yield(); //For Activate Loading Bar. Without it loading bar not Wokring.
                UnityWebRequestAsyncOperation asyncOperation = uwr.SendWebRequest();
                double fileSize = uwr.uploadHandler.data.LongLength;
                fileSize = Math.Round(fileSize / 1024 / 1024, 2);
                while (uwr.result == UnityWebRequest.Result.InProgress)
                {
                    progressBar.Value = asyncOperation.progress;
                    double uploadedSize = uwr.uploadedBytes;
                    progressBar.statusText.text = string.Format("{0}'s file is uploading {1} MB of {2} MB to server", fileName, Math.Round((uploadedSize / 1024 / 1024), 2), fileSize);
                    await Task.Yield();
                }
                /// Avoiding UnityWebRequest.Result.InProgress 
                Action apiCallBack = uwr.result switch
                {
                    UnityWebRequest.Result.ConnectionError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Connection Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.ProtocolError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Protocol Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.DataProcessingError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.Success => () =>
                    {
                        try
                        {
                            string response = Encoding.UTF8.GetString(uwr.downloadHandler.data);
                            if (showDebug)
                                Debug.Log(string.Format("Post File to Endpoint:-\n{0}\n\n is successfull", endPoint));
                            callBack?.Invoke(false, uwr.responseCode, string.Format("File - {0} Uploaded Successfully", fileName), response);
                        }
                        catch (Exception exception)
                        {
                            callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", "Response Parse Error on Client Side");
                            if (showDebug)
                                Debug.LogError(exception);
                        }
                    }
                    ,
                    _ => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with unknown error\nGet Request - Endpoint \n\n{0}\n\n is hitted\n\n Response:-\n\n{1}\n\n", endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Unknown Error", "Unknown Error");
                    }
                };
                progressBar.Reset();
                apiCallBack?.Invoke();
                uwr.Dispose();
            }
        }
        public async void PostFileThread(string endPoint, string accessToken, List<IMultipartFormSection> formData, string fileName, ProgressBar progressBar, bool showDebug, Action<bool, long, string, string> callBack)
        {
            if (showDebug)
                Debug.Log(string.Format("Post File to Endpoint:-\n{0}\n\n", endPoint));
            using (UnityWebRequest uwr = UnityWebRequest.Post(endPoint, formData))
            {
                uwr.disposeCertificateHandlerOnDispose = uwr.disposeDownloadHandlerOnDispose = uwr.disposeUploadHandlerOnDispose = true;
                progressBar.gameObject.SetActive(true);
                uwr.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                await Task.Yield(); //For Activate Loading Bar. Without it loading bar not Wokring.
                UnityWebRequestAsyncOperation asyncOperation = uwr.SendWebRequest();
                double fileSize = uwr.uploadHandler.data.LongLength;
                fileSize = Math.Round(fileSize / 1024 / 1024, 2);
                while (uwr.result == UnityWebRequest.Result.InProgress)
                {
                    progressBar.Value = asyncOperation.progress;
                    double uploadedSize = uwr.uploadedBytes;
                    progressBar.statusText.text = string.Format("{0}'s file is uploading {1} MB of {2} MB to server", fileName, Math.Round((uploadedSize / 1024 / 1024), 2), fileSize);
                    await Task.Yield();
                }
                /// Avoiding UnityWebRequest.Result.InProgress 
                Action apiCallBack = uwr.result switch
                {
                    UnityWebRequest.Result.ConnectionError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Connection Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.ProtocolError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Protocol Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.DataProcessingError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.Success => () =>
                    {
                        try
                        {
                            string response = Encoding.UTF8.GetString(uwr.downloadHandler.data);
                            if (showDebug)
                                Debug.Log(string.Format("Post File to Endpoint:-\n{0}\n\n is successfull", endPoint));
                            callBack?.Invoke(false, uwr.responseCode, string.Format("File - {0} Uploaded Successfully", fileName), response);
                        }
                        catch (Exception exception)
                        {
                            callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", "Response Parse Error on Client Side");
                            if (showDebug)
                                Debug.LogError(exception);
                        }
                    }
                    ,
                    _ => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with unknown error\nGet Request - Endpoint \n\n{0}\n\n is hitted\n\n Response:-\n\n{1}\n\n", endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Unknown Error", "Unknown Error");
                    }
                };
                progressBar.Reset();
                apiCallBack?.Invoke();
                uwr.Dispose();
            }
        }
        public async void PostJsonRequestThread(string endPoint, string accessToken, string jsonString, string dataName, bool showDebug, Action<bool, long, string, string> callBack)
        {
            if (showDebug)
                Debug.Log(string.Format("Post Request Endpoint:-\n{0}\nHitted with JSON:-\n\n{1}\n\n", endPoint, jsonString));
            using (UnityWebRequest uwr = UnityWebRequest.PostWwwForm(endPoint, jsonString))
            {
                uwr.disposeCertificateHandlerOnDispose = uwr.disposeDownloadHandlerOnDispose = uwr.disposeUploadHandlerOnDispose = true;
                progressBar.gameObject.SetActive(true);
                uwr.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                uwr.SetRequestHeader("Content-Type", "application/json");
                uwr.SetRequestHeader("Accept", "application/json");
                uwr.uploadHandler.contentType = "application/json";
                await Task.Yield(); //For Activate Loading Bar. Without it loading bar not Wokring.
                uwr.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(jsonString));
                UnityWebRequestAsyncOperation asyncOperation = uwr.SendWebRequest();
                double fileSize = uwr.uploadHandler.data.LongLength;
                fileSize = Math.Round(fileSize / 1024, 2);
                while (uwr.result == UnityWebRequest.Result.InProgress)
                {
                    progressBar.Value = asyncOperation.progress;
                    double uploadedSize = uwr.uploadedBytes;
                    progressBar.statusText.text = string.Format("{0}'s data is saving to server", dataName);
                    await Task.Yield();
                }
                /// Avoiding UnityWebRequest.Result.InProgress 
                Action apiCallBack = uwr.result switch
                {
                    UnityWebRequest.Result.ConnectionError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Connection Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.ProtocolError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Protocol Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.DataProcessingError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.Success => () =>
                    {
                        try
                        {
                            jsonString = Encoding.UTF8.GetString(uwr.downloadHandler.data);
                            if (showDebug)
                                Debug.Log(string.Format("Post Request - Endpoint \n\n{0}\n\n is hitted - Received Json\n\n{1}\n\n", endPoint, jsonString));
                            callBack?.Invoke(false, uwr.responseCode, string.Format("{0} is complete", dataName), jsonString);
                        }
                        catch (Exception exception)
                        {
                            callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", "Response Parse Error on Client Side");
                            if (showDebug)
                                Debug.LogError(exception);
                        }
                    }
                    ,
                    _ => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with unknown error\nGet Request - Endpoint \n\n{0}\n\n is hitted\n\n Response:-\n\n{1}\n\n", endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Unknown Error", "Unknown Error");
                    }
                    ,
                };
                progressBar.Reset();
                apiCallBack?.Invoke();
                uwr.Dispose();
            }
        }
        public async void PostJsonRequestThread(string endPoint, string accessToken, string jsonString, string dataName, ProgressBar progressBar, bool showDebug, Action<bool, long, string, string> callBack)
        {
            if (showDebug)
                Debug.Log(string.Format("Post Request Endpoint:-\n{0}\nHitted with JSON:-\n\n{1}\n\n", endPoint, jsonString));
            using (UnityWebRequest uwr = UnityWebRequest.PostWwwForm(endPoint, jsonString))
            {
                uwr.disposeCertificateHandlerOnDispose = uwr.disposeDownloadHandlerOnDispose = uwr.disposeUploadHandlerOnDispose = true;
                progressBar.gameObject.SetActive(true);
                uwr.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                uwr.SetRequestHeader("Content-Type", "application/json");
                uwr.SetRequestHeader("Accept", "application/json");
                uwr.uploadHandler.contentType = "application/json";
                await Task.Yield(); //For Activate Loading Bar. Without it loading bar not Wokring.
                uwr.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(jsonString));
                UnityWebRequestAsyncOperation asyncOperation = uwr.SendWebRequest();
                double fileSize = uwr.uploadHandler.data.LongLength;
                fileSize = Math.Round(fileSize / 1024, 2);
                while (uwr.result == UnityWebRequest.Result.InProgress)
                {
                    progressBar.Value = asyncOperation.progress;
                    double uploadedSize = uwr.uploadedBytes;
                    progressBar.statusText.text = string.Format("{0}'s data is saving to server", dataName);
                    await Task.Yield();
                }
                /// Avoiding UnityWebRequest.Result.InProgress 
                Action apiCallBack = uwr.result switch
                {
                    UnityWebRequest.Result.ConnectionError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Connection Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.ProtocolError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Protocol Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.DataProcessingError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.Success => () =>
                    {
                        try
                        {
                            jsonString = Encoding.UTF8.GetString(uwr.downloadHandler.data);
                            if (showDebug)
                                Debug.Log(string.Format("Post Request - Endpoint \n\n{0}\n\n is hitted - Received Json\n\n{1}\n\n", endPoint, jsonString));
                            callBack?.Invoke(false, uwr.responseCode, string.Format("{0} is complete", dataName), jsonString);
                        }
                        catch (Exception exception)
                        {
                            callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", "Response Parse Error on Client Side");
                            if (showDebug)
                                Debug.LogError(exception);
                        }
                    }
                    ,
                    _ => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with unknown error\nGet Request - Endpoint \n\n{0}\n\n is hitted\n\n Response:-\n\n{1}\n\n", endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Unknown Error", "Unknown Error");
                    }
                    ,
                };
                progressBar.Reset();
                apiCallBack?.Invoke();
                uwr.Dispose();
            }
        }
        #endregion Post Methods

        #region Put Method
        public async void PutJsonRequestThread(string endPoint, string accessToken, string jsonString, string dataName, bool showDebug, Action<bool, long, string, string> callBack)
        {
            if (showDebug)
                Debug.Log(string.Format("Put Request Endpoint:-\n{0}\nHitted with JSON:-\n\n{1}\n\n", endPoint, jsonString));
            using (UnityWebRequest uwr = UnityWebRequest.Put(endPoint, jsonString))
            {
                uwr.disposeCertificateHandlerOnDispose = uwr.disposeDownloadHandlerOnDispose = uwr.disposeUploadHandlerOnDispose = true;
                //uwr.method = string.Format("PATCH");
                progressBar.gameObject.SetActive(true);
                uwr.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                uwr.SetRequestHeader("Content-Type", "application/json");
                uwr.SetRequestHeader("Accept", "application/json");
                uwr.uploadHandler.contentType = "application/json";
                await Task.Yield(); //For Activate Loading Bar. Without it loading bar not Wokring.
                uwr.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(jsonString));
                UnityWebRequestAsyncOperation asyncOperation = uwr.SendWebRequest();
                double fileSize = uwr.uploadHandler.data.LongLength;
                fileSize = Math.Round(fileSize / 1024, 2);
                while (uwr.result == UnityWebRequest.Result.InProgress)
                {
                    progressBar.Value = asyncOperation.progress;
                    double uploadedSize = uwr.uploadedBytes;
                    progressBar.statusText.text = string.Format("{0}'s data is updating to server", dataName);
                    await Task.Yield();
                }
                /// Avoiding UnityWebRequest.Result.InProgress 
                Action apiCallBack = uwr.result switch
                {
                    UnityWebRequest.Result.ConnectionError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Connection Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.ProtocolError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Protocol Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.DataProcessingError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.Success => () =>
                    {
                        try
                        {
                            jsonString = Encoding.UTF8.GetString(uwr.downloadHandler.data);
                            if (showDebug)
                                Debug.Log(string.Format("Put Request - Endpoint \n\n{0}\n\n is hitted - Received Json\n\n{1}\n\n", endPoint, jsonString));
                            callBack?.Invoke(false, uwr.responseCode, string.Format("{0} is complete", dataName), jsonString);
                        }
                        catch (Exception exception)
                        {
                            callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", "Response Parse Error on Client Side");
                            if (showDebug)
                                Debug.LogError(exception);
                        }
                    }
                    ,
                    _ => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with unknown error\nGet Request - Endpoint \n\n{0}\n\n is hitted\n\n Response:-\n\n{1}\n\n", endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Unknown Error", "Unknown Error");
                    }
                    ,
                };
                progressBar.Reset();
                apiCallBack?.Invoke();
                uwr.Dispose();
            }
        }
        public async void PutJsonRequestThread(string endPoint, string accessToken, string jsonString, ProgressBar progressBar, string dataName, bool showDebug, Action<bool, long, string, string> callBack)
        {
            if (showDebug)
                Debug.Log(string.Format("Put Request Endpoint:-\n{0}\nHitted with JSON:-\n\n{1}\n\n", endPoint, jsonString));
            using (UnityWebRequest uwr = UnityWebRequest.Put(endPoint, jsonString))
            {
                uwr.disposeCertificateHandlerOnDispose = uwr.disposeDownloadHandlerOnDispose = uwr.disposeUploadHandlerOnDispose = true;
                //uwr.method = string.Format("PATCH");
                progressBar.gameObject.SetActive(true);
                uwr.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                uwr.SetRequestHeader("Content-Type", "application/json");
                uwr.SetRequestHeader("Accept", "application/json");
                uwr.uploadHandler.contentType = "application/json";
                await Task.Yield(); //For Activate Loading Bar. Without it loading bar not Wokring.
                uwr.uploadHandler = new UploadHandlerRaw(Encoding.UTF8.GetBytes(jsonString));
                UnityWebRequestAsyncOperation asyncOperation = uwr.SendWebRequest();
                double fileSize = uwr.uploadHandler.data.LongLength;
                fileSize = Math.Round(fileSize / 1024, 2);
                while (uwr.result == UnityWebRequest.Result.InProgress)
                {
                    progressBar.Value = asyncOperation.progress;
                    double uploadedSize = uwr.uploadedBytes;
                    progressBar.statusText.text = string.Format("{0}'s data is updating to server", dataName);
                    await Task.Yield();
                }
                /// Avoiding UnityWebRequest.Result.InProgress 
                Action apiCallBack = uwr.result switch
                {
                    UnityWebRequest.Result.ConnectionError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Connection Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.ProtocolError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Protocol Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.DataProcessingError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.Success => () =>
                    {
                        try
                        {
                            jsonString = Encoding.UTF8.GetString(uwr.downloadHandler.data);
                            if (showDebug)
                                Debug.Log(string.Format("Put Request - Endpoint \n\n{0}\n\n is hitted - Received Json\n\n{1}\n\n", endPoint, jsonString));
                            callBack?.Invoke(false, uwr.responseCode, string.Format("{0} is complete", dataName), jsonString);
                        }
                        catch (Exception exception)
                        {
                            callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", "Response Parse Error on Client Side");
                            if (showDebug)
                                Debug.LogError(exception);
                        }
                    }
                    ,
                    _ => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with unknown error\nGet Request - Endpoint \n\n{0}\n\n is hitted\n\n Response:-\n\n{1}\n\n", endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Unknown Error", "Unknown Error");
                    }
                    ,
                };
                progressBar.Reset();
                apiCallBack?.Invoke();
                uwr.Dispose();
            }
        }
        #endregion Put Method

        #region Get Methods
        public async void GetFileThread(string endPoint, string accessToken, string fileName, bool showDebug, Action<bool, long, string, string, byte[]> callBack)
        {
            if (showDebug)
                Debug.Log(string.Format("Get File from - Endpoint \n\n{0}\n\n is hitted", endPoint));
            using (UnityWebRequest uwr = UnityWebRequest.Get(endPoint))
            {
                uwr.disposeCertificateHandlerOnDispose = uwr.disposeDownloadHandlerOnDispose = uwr.disposeUploadHandlerOnDispose = true;
                progressBar.gameObject.SetActive(true);
                progressBar.statusText.text = string.Format("Downloading {0}", fileName);
                uwr.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                await Task.Yield(); //For Activate Loading Bar. Without it loading bar not Wokring.
                double fileSize = 0;
                using (UnityWebRequest request = UnityWebRequest.Head(endPoint))
                {
                    request.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                    await Task.Yield();
                    double.TryParse(request.GetResponseHeader("Content-Length"), out fileSize);
                    request.Dispose();
                }
                fileSize = Math.Round(fileSize / 1024 / 1024, 2);
                UnityWebRequestAsyncOperation asyncOperation = uwr.SendWebRequest();
                while (uwr.result == UnityWebRequest.Result.InProgress)
                {
                    progressBar.Value = uwr.downloadProgress;
                    double downloadedSize = uwr.downloadedBytes;
                    progressBar.statusText.text = string.Format("{0}'s file is downloading {1} MB of {2} MB from server", fileName, Math.Round((downloadedSize / 1024 / 1024), 2), fileSize);
                    await Task.Yield();
                }
                /// Avoiding UnityWebRequest.Result.InProgress 
                Action apiCallBack = uwr.result switch
                {
                    UnityWebRequest.Result.ConnectionError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Connection Error", uwr.error, null);
                    }
                    ,
                    UnityWebRequest.Result.ProtocolError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Protocol Error", uwr.error, null);
                    }
                    ,
                    UnityWebRequest.Result.DataProcessingError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", uwr.error, null);
                    }
                    ,
                    UnityWebRequest.Result.Success => () =>
                    {
                        try
                        {
                            byte[] data = uwr.downloadHandler.data;
                            if (showDebug)
                                Debug.Log(string.Format("Get File from Endpoint:-\n{0}\n\n is successfull", endPoint));
                            callBack?.Invoke(false, uwr.responseCode, "Successful", "File Downloaded Successfully", data);
                        }
                        catch (Exception exception)
                        {
                            callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", "Response Parse Error on Client Side", null);
                            if (showDebug)
                                Debug.LogError(exception);
                        }
                    }
                    ,
                    _ => () =>
                    {
                        callBack?.Invoke(true, uwr.responseCode, "Unknown Error", "Unknown Error", null);
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with unknown error\nGet Request - Endpoint \n\n{0}\n\n is hitted\n\n Response:-\n\n{1}\n\n", endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                    }
                };
                progressBar.Reset();
                apiCallBack?.Invoke();
                uwr.Dispose();
            }
        }
        public async void GetFileThread(string endPoint, string accessToken, string fileName, ProgressBar progressBar, bool showDebug, Action<bool, long, string, string, byte[]> callBack)
        {
            if (showDebug)
                Debug.Log(string.Format("Get File from - Endpoint \n\n{0}\n\n is hitted", endPoint));
            using (UnityWebRequest uwr = UnityWebRequest.Get(endPoint))
            {
                uwr.disposeCertificateHandlerOnDispose = uwr.disposeDownloadHandlerOnDispose = uwr.disposeUploadHandlerOnDispose = true;
                progressBar.gameObject.SetActive(true);
                progressBar.statusText.text = string.Format("Downloading {0}", fileName);
                uwr.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                await Task.Yield(); //For Activate Loading Bar. Without it loading bar not Wokring.
                double fileSize = 0;
                using (UnityWebRequest request = UnityWebRequest.Head(endPoint))
                {
                    request.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                    await Task.Yield();
                    double.TryParse(request.GetResponseHeader("Content-Length"), out fileSize);
                    request.Dispose();
                }
                fileSize = Math.Round(fileSize / 1024 / 1024, 2);
                UnityWebRequestAsyncOperation asyncOperation = uwr.SendWebRequest();
                while (uwr.result == UnityWebRequest.Result.InProgress)
                {
                    progressBar.Value = uwr.downloadProgress;
                    double downloadedSize = uwr.downloadedBytes;
                    progressBar.statusText.text = string.Format("{0}'s file is downloading {1} MB of {2} MB from server", fileName, Math.Round((downloadedSize / 1024 / 1024), 2), fileSize);
                    await Task.Yield();
                }
                /// Avoiding UnityWebRequest.Result.InProgress 
                Action apiCallBack = uwr.result switch
                {
                    UnityWebRequest.Result.ConnectionError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Connection Error", uwr.error, null);
                    }
                    ,
                    UnityWebRequest.Result.ProtocolError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Protocol Error", uwr.error, null);
                    }
                    ,
                    UnityWebRequest.Result.DataProcessingError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", uwr.error, null);
                    }
                    ,
                    UnityWebRequest.Result.Success => () =>
                    {
                        try
                        {
                            byte[] data = uwr.downloadHandler.data;
                            if (showDebug)
                                Debug.Log(string.Format("Get File from Endpoint:-\n{0}\n\n is successfull", endPoint));
                            callBack?.Invoke(false, uwr.responseCode, "Successful", "File Downloaded Successfully", data);
                        }
                        catch (Exception exception)
                        {
                            callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", "Response Parse Error on Client Side", null);
                            if (showDebug)
                                Debug.LogError(exception);
                        }
                    }
                    ,
                    _ => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with unknown error\nGet Request - Endpoint \n\n{0}\n\n is hitted\n\n Response:-\n\n{1}\n\n", endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Unknown Error", "Unknown Error", null);
                    }
                };
                progressBar.Reset();
                apiCallBack?.Invoke();
                uwr.Dispose();
            }
        }
        public async void GetJsonResponseThread(string endPoint, string accessToken, string dataName, bool showDebug, Action<bool, long, string, string> callBack)
        {
            if (showDebug)
                Debug.Log(string.Format("Get Request - Endpoint \n\n{0}\n\n is hitted", endPoint));
            using (UnityWebRequest uwr = UnityWebRequest.Get(endPoint))
            {
                uwr.disposeCertificateHandlerOnDispose = uwr.disposeDownloadHandlerOnDispose = uwr.disposeUploadHandlerOnDispose = true;
                progressBar.gameObject.SetActive(true);
                progressBar.statusText.text = string.Format("Fetching {0}", dataName);
                uwr.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                uwr.SetRequestHeader("Accept", "application/json");
                await Task.Yield(); //For Activate Loading Bar. Without it loading bar not Wokring.                
                UnityWebRequestAsyncOperation asyncOperation = uwr.SendWebRequest();
                while (uwr.result == UnityWebRequest.Result.InProgress)
                {
                    progressBar.Value = asyncOperation.progress;
                    progressBar.statusText.text = string.Format("{0}'s data is downloading from server", dataName);
                    await Task.Yield();
                }
                /// Avoiding UnityWebRequest.Result.InProgress 
                Action apiCallBack = uwr.result switch
                {
                    UnityWebRequest.Result.ConnectionError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Connection Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.ProtocolError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Protocol Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.DataProcessingError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.Success => () =>
                    {
                        try
                        {
                            string jsonString = Encoding.UTF8.GetString(uwr.downloadHandler.data);
                            if (showDebug)
                                Debug.Log(string.Format("Get Request - Endpoint \n\n{0}\n\n is hitted - Received Json\n\n{1}\n\n", endPoint, jsonString));
                            callBack?.Invoke(false, uwr.responseCode, string.Format("{0} is complete", dataName), jsonString);
                        }
                        catch (Exception exception)
                        {
                            callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", "Response Parse Error on Client Side");
                            if (showDebug)
                                Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", endPoint, exception, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        }
                    }
                    ,
                    _ => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with unknown error\nGet Request - Endpoint \n\n{0}\n\n is hitted\n\n Response:-\n\n{1}\n\n", endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Unknown Error", "Unknown Error");
                    }
                    ,
                };
                progressBar.Reset();
                apiCallBack?.Invoke();
                uwr.Dispose();
            }
        }
        public async void GetJsonResponseThread(string endPoint, string accessToken, string dataName, ProgressBar progressBar, bool showDebug, Action<bool, long, string, string> callBack)
        {
            if (showDebug)
                Debug.Log(string.Format("Get Request - Endpoint \n\n{0}\n\n is hitted", endPoint));
            using (UnityWebRequest uwr = UnityWebRequest.Get(endPoint))
            {
                uwr.disposeCertificateHandlerOnDispose = uwr.disposeDownloadHandlerOnDispose = uwr.disposeUploadHandlerOnDispose = true;
                progressBar.gameObject.SetActive(true);
                progressBar.statusText.text = string.Format("Fetching {0}", dataName);
                uwr.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                uwr.SetRequestHeader("Accept", "application/json");
                await Task.Yield(); //For Activate Loading Bar. Without it loading bar not Wokring.                
                UnityWebRequestAsyncOperation asyncOperation = uwr.SendWebRequest();
                while (uwr.result == UnityWebRequest.Result.InProgress)
                {
                    progressBar.Value = asyncOperation.progress;
                    progressBar.statusText.text = string.Format("{0}'s data is downloading from server", dataName);
                    await Task.Yield();
                }
                /// Avoiding UnityWebRequest.Result.InProgress 
                Action apiCallBack = uwr.result switch
                {
                    UnityWebRequest.Result.ConnectionError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Connection Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.ProtocolError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Protocol Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.DataProcessingError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.Success => () =>
                    {
                        try
                        {
                            string jsonString = Encoding.UTF8.GetString(uwr.downloadHandler.data);
                            if (showDebug)
                                Debug.Log(string.Format("Get Request - Endpoint \n\n{0}\n\n is hitted - Received Json\n\n{1}\n\n", endPoint, jsonString));
                            callBack?.Invoke(false, uwr.responseCode, string.Format("{0} is complete", dataName), jsonString);
                        }
                        catch (Exception exception)
                        {
                            callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", "Response Parse Error on Client Side");
                            if (showDebug)
                                Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", endPoint, exception, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        }
                    }
                    ,
                    _ => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with unknown error\nGet Request - Endpoint \n\n{0}\n\n is hitted\n\n Response:-\n\n{1}\n\n", endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Unknown Error", "Unknown Error");
                    }
                    ,
                };
                progressBar.Reset();
                apiCallBack?.Invoke();
                uwr.Dispose();
            }
        }
        #endregion Get Methods

        #region Delete Method
        public async void DeleteDataThread(string endPoint, string accessToken, string dataName, bool showDebug, Action<bool, long, string, string> callBack)
        {
            if (showDebug)
                Debug.Log(string.Format("Delete Request - Endpoint \n\n{0}\n\n is hitted", endPoint));
            using (UnityWebRequest uwr = UnityWebRequest.Delete(endPoint))
            {
                uwr.disposeCertificateHandlerOnDispose = uwr.disposeDownloadHandlerOnDispose = uwr.disposeUploadHandlerOnDispose = true;
                progressBar.gameObject.SetActive(true);
                uwr.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                uwr.SetRequestHeader("Accept", "application/json");
                await Task.Yield(); //For Activate Loading Bar. Without it loading bar not Wokring.
                UnityWebRequestAsyncOperation asyncOperation = uwr.SendWebRequest();
                while (uwr.result == UnityWebRequest.Result.InProgress)
                {
                    progressBar.Value = asyncOperation.progress;
                    progressBar.statusText.text = string.Format("{0}'s data is deleting on server", dataName);
                    await Task.Yield();
                }
                /// Avoiding UnityWebRequest.Result.InProgress 
                Action apiCallBack = uwr.result switch
                {
                    UnityWebRequest.Result.ConnectionError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Connection Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.ProtocolError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Protocol Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.DataProcessingError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.Success => () =>
                    {
                        try
                        {
                            if (uwr.responseCode == 204)
                            {
                                callBack?.Invoke(false, uwr.responseCode, "Delete Success", string.Format("{0} is complete", dataName));
                                if (showDebug)
                                    Debug.Log(string.Format("Delete Data from Endpoint:-\n{0}\n\n is successfull", endPoint));
                            }
                            else
                            {
                                callBack?.Invoke(true, uwr.responseCode, "Unknown Status", "Unknown Status for WebGL");
                                if (showDebug)
                                    Debug.LogWarning(string.Format("Delete Data from Endpoint:-\n{0}\n\n is in Unknown Status", endPoint));
                            }
                        }
                        catch (Exception exception)
                        {
                            callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", "Response Parse Error on Client Side");
                            if (showDebug)
                                Debug.LogError(exception);
                        }
                    }
                    ,
                    _ => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with unknown error\nGet Request - Endpoint \n\n{0}\n\n is hitted\n\n Response:-\n\n{1}\n\n", endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Unknown Error", "Unknown Error");
                    }
                    ,
                };
                progressBar.Reset();
                apiCallBack?.Invoke();
                uwr.Dispose();
            }
        }
        public async void DeleteDataThread(string endPoint, string accessToken, string dataName, ProgressBar progressBar, bool showDebug, Action<bool, long, string, string> callBack)
        {
            if (showDebug)
                Debug.Log(string.Format("Delete Request - Endpoint \n\n{0}\n\n is hitted", endPoint));
            using (UnityWebRequest uwr = UnityWebRequest.Delete(endPoint))
            {
                uwr.disposeCertificateHandlerOnDispose = uwr.disposeDownloadHandlerOnDispose = uwr.disposeUploadHandlerOnDispose = true;
                progressBar.gameObject.SetActive(true);
                uwr.SetRequestHeader("Authorization", string.IsNullOrEmpty(accessToken) ? "" : string.Format("Bearer {0}", accessToken));
                uwr.SetRequestHeader("Accept", "application/json");
                await Task.Yield(); //For Activate Loading Bar. Without it loading bar not Wokring.
                UnityWebRequestAsyncOperation asyncOperation = uwr.SendWebRequest();
                while (uwr.result == UnityWebRequest.Result.InProgress)
                {
                    progressBar.Value = asyncOperation.progress;
                    progressBar.statusText.text = string.Format("{0}'s data is deleting on server", dataName);
                    await Task.Yield();
                }
                /// Avoiding UnityWebRequest.Result.InProgress 
                Action apiCallBack = uwr.result switch
                {
                    UnityWebRequest.Result.ConnectionError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Connection Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.ProtocolError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Protocol Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.DataProcessingError => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with {0}\nGet Request - Endpoint \n\n{1}\n\n is hitted\n\n Response:-\n\n{2}\n\n", uwr.responseCode, endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", uwr.error);
                    }
                    ,
                    UnityWebRequest.Result.Success => () =>
                    {
                        try
                        {
                            if (uwr.responseCode == 204)
                            {
                                callBack?.Invoke(false, uwr.responseCode, "Delete Success", string.Format("{0} is complete", dataName));
                                if (showDebug)
                                    Debug.Log(string.Format("Delete Data from Endpoint:-\n{0}\n\n is successfull", endPoint));
                            }
                            else
                            {
                                callBack?.Invoke(true, uwr.responseCode, "Unknown Status", "Unknown Status for WebGL");
                                if (showDebug)
                                    Debug.LogWarning(string.Format("Delete Data from Endpoint:-\n{0}\n\n is in Unknown Status", endPoint));
                            }
                        }
                        catch (Exception exception)
                        {
                            callBack?.Invoke(true, uwr.responseCode, "Data Processing Error", "Response Parse Error on Client Side");
                            if (showDebug)
                                Debug.LogError(exception);
                        }
                    }
                    ,
                    _ => () =>
                    {
                        if (showDebug)
                            Debug.LogError(string.Format("Error Occured with unknown error\nGet Request - Endpoint \n\n{0}\n\n is hitted\n\n Response:-\n\n{1}\n\n", endPoint, Encoding.UTF8.GetString(uwr.downloadHandler.data)));
                        callBack?.Invoke(true, uwr.responseCode, "Unknown Error", "Unknown Error");
                    }
                    ,
                };
                progressBar.Reset();
                apiCallBack?.Invoke();
                uwr.Dispose();
            }
        }
        #endregion Delete Method

        #endregion Asynchronous Threads

        #endregion Public Methods
    }
}