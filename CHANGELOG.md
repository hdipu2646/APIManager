[v3.0.0](#v3.0.0) - 26th September 2022
### Upgradations
- Progress Bar New Package Added
- Singleton new system added
- Packages Dependency added

### Changes
- Input System may have been changed to Unity New Input System


[v2.1.4](#v2.1.4) - 26th September 2022
### Added
- Nothing

### Changes
- Remove Head Request from GetRequestAsync and GetRequestThread

[v2.1.3](#v2.1.3) - 13th September 2022
### Added
- Optimize file getting 

### Changes
- asyncronous progress now change with downloadprogress of Get File system

[v2.1.2](#v2.1.2) - 13th September 2022
### Added
- Optimize Api Call
- Git Ignore updates

### Changes
- Progress Bar Loading Text is now more relevant

[v2.1.1](#v2.1.1) - 05th September 2022
### Added
- Progress Bar Text Message added before file Size fetched.

### Changes
- Progress Bar Text Empty fixed.

[v2.1.0](#v2.1.0) - 05th September 2022
### Added
- GetFile actual File Size on server is now calculated.

### Changes
- Response Size also calculated on Get request methods.

[v2.0.1](#v2.0.1) - 25th August 2022
### Added
- API Calling On Error Event Upgraded
- API Calling On Error Event Example Added
- API Calling On Error Event Clear Functionality Added

### Changes
- API Calling On Error Event Handling Upgraded

[v2.0.0](#v2.0.0) - 07th August 2022
### Added
- API Call with Thread added
- User can Now add Custom Progress bar

### Changes
- API Coroutine Call Optimized

[v1.0.0](#v1.0.0)